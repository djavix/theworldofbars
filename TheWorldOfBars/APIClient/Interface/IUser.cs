﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APIClient.Interface
{
    public interface IUser : IBase<Common.API.RUser>
    {
        Task<Common.API.Response<Common.API.RUser>> Login(Common.Entities.User user);
    }
}
