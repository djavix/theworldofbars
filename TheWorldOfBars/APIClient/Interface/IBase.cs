﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APIClient.Interface
{
    public interface IBase<TEntity>
    {
        Task<Common.API.Response<TEntity>> Add(TEntity entity);
        Task<Common.API.Response<TEntity>> Update(TEntity entity);
        Task<Common.API.Response<TEntity>> Delete(TEntity entity);
        Task<Common.API.Response<TEntity>> SearchById(TEntity entity);
        Task<Common.API.Response<TEntity>> SearchAll();

    }
}
