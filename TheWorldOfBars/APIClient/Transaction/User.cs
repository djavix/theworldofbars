﻿using APIClient.Interface;
using Common.API;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace APIClient.Transaction
{
    internal class User : Base<Common.API.RUser>, IUser
    {
        public User() : base()
        {
            _url += this.GetType().Name.ToLower();
        }

        public async Task<Response<RUser>> Login(Common.Entities.User user)
        {
            _url += "/login";
            _client.BaseAddress = new Uri(_url);
            HttpResponseMessage response = await _client.PostAsJsonAsync("", user);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<Response<RUser>>();
        }
    }
}
