﻿using APIClient.Interface;
using Common.API;
using Common.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace APIClient.Transaction
{
    internal class Base<TEntity> : IBase<TEntity> where TEntity : REntity
    {
        protected string _url;
        protected HttpClient _client;
        public Base()
        {
            _url = @"http://192.168.1.78:9090/api/";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<Response<TEntity>> Add(TEntity entity)
        {
            _url += "/add";
            _client.BaseAddress = new Uri(_url);
            HttpResponseMessage response = await _client.PostAsJsonAsync("", entity);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<Response<TEntity>>();
        }

        public async Task<Response<TEntity>> Delete(TEntity entity)
        {
            _url += "/delete";
            _client.BaseAddress = new Uri(_url);
            HttpResponseMessage response = await _client.PostAsJsonAsync("", entity);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<Response<TEntity>>();
        }

        public async Task<Response<TEntity>> SearchAll()
        {
            _client.BaseAddress = new Uri(_url);
            HttpResponseMessage response = await _client.GetAsync("");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<Response<TEntity>>();          
        }

        public async Task<Response<TEntity>> SearchById(TEntity entity)
        {
            _url += "/searchbyid";
            _client.BaseAddress = new Uri(_url);
            HttpResponseMessage response = await _client.PostAsJsonAsync("", entity);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<Response<TEntity>>();
        }

        public async Task<Response<TEntity>> Update(TEntity entity)
        {
            _url += "/update";
            _client.BaseAddress = new Uri(_url);
            HttpResponseMessage response = await _client.PostAsJsonAsync("", entity);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<Response<TEntity>>();
        }
    }
}
