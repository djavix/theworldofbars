﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Command.User
{
    internal class SearchAll : Command<int, IList<Common.Entities.User>>
    {
        private DAO.IDAOS.IUser _dao;
        public override async Task ExecuteAsync()
        {
            _dao = DAO.Factory.GetDAOUser();
            _output = await _dao.SearchAllAsync();
        }
    }
}
