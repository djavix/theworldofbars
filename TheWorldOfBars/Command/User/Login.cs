﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Command.User
{
    internal class Login : Command<Common.Entities.User, Common.Entities.User>
    {
        private DAO.IDAOS.IUser _dao;
        public override async Task ExecuteAsync()
        {
            _dao = DAO.Factory.GetDAOUser();
            _output = await _dao.Login(_input);
        }
    }
}
