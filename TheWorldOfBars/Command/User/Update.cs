﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Command.User
{
    internal class Update : Command<Common.Entities.User, bool>
    {
        private DAO.IDAOS.IUser _dao;
        public override async Task ExecuteAsync()
        {
            _dao = DAO.Factory.GetDAOUser();
            _output = await _dao.UpdateAsync(_input);
        }
    }
}
