﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Command
{
    public static class Factory
    {
        #region User
        public static Command<Common.Entities.User,bool> GetCommandUserAdd()
        {
            return new User.Add();
        }
        public static Command<Common.Entities.User,bool> GetCommandUserDelete()
        {
            return new User.Delete();
        }
        public static Command<Common.Entities.User,Common.Entities.User> GetCommandUserLogin()
        {
            return new User.Login();
        }
        public static Command<int,IList<Common.Entities.User>> GetCommandUserSearchAll()
        {
            return new User.SearchAll();
        }
        public static Command<Common.Entities.User,Common.Entities.User> GetCommandUserSearchById()
        {
            return new User.SearchById();
        }
        public static Command<Common.Entities.User,bool> GetCommandUserUpdate()
        {
            return new User.Update();
        }
        #endregion
    }
}
