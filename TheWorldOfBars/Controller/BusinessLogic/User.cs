﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Controller.BusinessLogic
{
    internal class User
    {
        public User() { }

        public async Task<bool> Add(Common.Entities.User user)
        {
            bool result = false;

            try
            {
                Command.Command<Common.Entities.User, bool> command = Command.Factory.GetCommandUserAdd();
                command.Input = user;
                await command.ExecuteAsync();
                result = command.Output;
            }
            catch(Exception ex)
            {

            }
            return result;
        }

        public async Task<Common.Entities.User> Login(Common.Entities.User user)
        {
            Common.Entities.User result = null;

            try
            {
                Command.Command<Common.Entities.User, Common.Entities.User> command = Command.Factory.GetCommandUserLogin();
                command.Input = user;
                await command.ExecuteAsync();
                result = command.Output;
            }
            catch(Exception ex)
            {

            }

            return result;
        }

        public async Task<IList<Common.Entities.User>> SearchAll()
        {
            IList<Common.Entities.User> result = null;

            try
            {
                Command.Command<int, IList<Common.Entities.User>> command = Command.Factory.GetCommandUserSearchAll();
                await command.ExecuteAsync();
                result = command.Output;
            }
            catch (Exception ex)
            {

            }

            return result;
        }


    }
}
