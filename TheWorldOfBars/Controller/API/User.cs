﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Controller.API
{
    public class User
    {
        private BusinessLogic.User _user;

        public User()
        {
            _user = new BusinessLogic.User();
        }

        public async Task<Common.API.Response<Common.API.RUser>> GetResponseAdd(Common.Entities.User user)
        {
            Common.API.Response<Common.API.RUser> response = new Common.API.Response<Common.API.RUser>();

            try
            {
                if (await _user.Add(user))
                    response.StatusOperation = Common.Enums.StatusOperation.Success;
                else
                    response.StatusOperation = Common.Enums.StatusOperation.Fail;

                response.StatusAPI = Common.Enums.APICode.Ok;
            }
            catch(Exception ex)
            {
                response.StatusAPI = Common.Enums.APICode.InternalError;                
            }

            return response;
        }

        public async Task<Common.API.Response<Common.API.RUser>> GetResponseLogin(Common.Entities.User user)
        {
            Common.API.Response<Common.API.RUser> response = new Common.API.Response<Common.API.RUser>();

            try
            {
                Common.Entities.User login = await _user.Login(user);
                if (login != null)
                {
                    response.StatusOperation = Common.Enums.StatusOperation.Success;
                    response.Entity = (Common.API.RUser)login;
                }
                else
                    response.StatusOperation = Common.Enums.StatusOperation.Fail;

                response.StatusAPI = Common.Enums.APICode.Ok;
            }
            catch (Exception ex)
            {
                response.StatusAPI = Common.Enums.APICode.InternalError;
            }

            return response;
        }

        public async Task<Common.API.Response<Common.API.RUser>> GetResponseSearchAll()
        {
            Common.API.Response<Common.API.RUser> response = new Common.API.Response<Common.API.RUser>();

            try
            {
                IList<Common.Entities.User> users = await _user.SearchAll();
                if (users != null && users.Count > 0)
                {
                    response.StatusOperation = Common.Enums.StatusOperation.Success;
                    response.Entities = new List<Common.API.RUser>();
                    foreach (Common.Entities.User user in users)
                        response.Entities.Add((Common.API.RUser)user);
                }
                else
                    response.StatusOperation = Common.Enums.StatusOperation.Fail;
                response.StatusAPI = Common.Enums.APICode.Ok;
            }
            catch (Exception ex)
            {
                response.StatusAPI = Common.Enums.APICode.InternalError;
            }

            return response;
        }
    }
}
