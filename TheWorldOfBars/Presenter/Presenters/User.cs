﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Presenter.Presenters
{
    public class User
    {
        private Presenter.IPresenters.IUser _viewModel;
        private APIClient.Interface.IUser _userService;

        public User(Presenter.IPresenters.IUser viewModel)
        {
            _viewModel = viewModel;
        }

        private bool ValidateUser(bool validateAll)
        {
            bool result = true;
            if (_viewModel.User != null)
            {
                if (string.IsNullOrEmpty(_viewModel.User.NickName) || string.IsNullOrEmpty(_viewModel.User.Password))
                    result = false;

                if (validateAll)
                    if (string.IsNullOrEmpty(_viewModel.User.FullName) || string.IsNullOrEmpty(_viewModel.User.Email) || string.IsNullOrEmpty(_viewModel.User.Birthday.ToLongDateString()))
                        result = false;
            }
            else result = false;

            if (!result)
                _viewModel.Message = "All fields are required.";
            
            return result;
        }

        public async Task<bool> Save()
        {
            bool result = false;
            try
            {
                if(ValidateUser(true))
                {
                    _userService = APIClient.Factory.GetAPIClientUser();
                    //Common.API.Response<Common.API.RUser> response = await _userService.Add((Common.API.RUser)_viewModel.User);
                    Common.API.Response<Common.API.RUser> response = await _userService.SearchAll();
                    if (response != null && response.StatusAPI == Common.Enums.APICode.Ok && response.StatusOperation == Common.Enums.StatusOperation.Success)
                        result = true;
                    else
                        _viewModel.Message = response.Message;
                }
            }
            catch(Exception ex)
            {
                _viewModel.Message = "Could not connect.";
            }
            return result;
        }
    }
}
