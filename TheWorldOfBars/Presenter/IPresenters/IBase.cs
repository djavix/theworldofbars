﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Presenter.IPresenters
{
    public interface IBase
    {
        string Message { set; }
    }
}
