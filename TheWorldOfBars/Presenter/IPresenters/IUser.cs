﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Presenter.IPresenters
{
    public interface IUser : IBase
    {
        Common.Entities.User User { get; set; }
    }
}
