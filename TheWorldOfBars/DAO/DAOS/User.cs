﻿using Common.Entities;
using DAO.IDAOS;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAO.DAOS
{
    internal class User : Base<Common.Entities.User>, IUser
    {
        public async Task<Common.Entities.User> Login(Common.Entities.User user)
        {
            Common.Entities.User result = null;
            try
            {
                result = await _session.QueryOver<Common.Entities.User>().Where(u => u.NickName == user.NickName).And(u => u.Password == user.Password).SingleOrDefaultAsync();
            }
            catch(Exception ex)
            {

            }
            return result;
        }
    }
}
 