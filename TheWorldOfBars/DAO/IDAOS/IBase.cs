﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.IDAOS
{
    public interface IBase<TEntity>
    {
        /// <summary>
        /// Firma de metodo agregar
        /// </summary>
        /// <param name="entity">endidad agregar</param>
        /// <returns>booleano de la operacion</returns>
        System.Threading.Tasks.Task<bool> AddAsync(TEntity entity);
        /// <summary>
        /// Firma de metodo actualiuzar
        /// </summary>
        /// <param name="entity">entidad actualizar</param>
        /// <returns>booleano de la operacion </returns>
        System.Threading.Tasks.Task<bool> UpdateAsync(TEntity entity);
        /// <summary>
        /// Firma de metodo eliminar
        /// </summary>
        /// <param name="entity">entidad a eliminar</param>
        /// <returns>booleano de la operacion</returns>
        System.Threading.Tasks.Task<bool> DeleteAsync(TEntity entity);
        /// <summary>
        /// Firma de metodo para buscar por id
        /// </summary>
        /// <param name="id">id de la entidad a buscar</param>
        /// <returns>entidad solicitada</returns>
        System.Threading.Tasks.Task<TEntity> SearchByIdAsync(TEntity entity);
        /// <summary>
        /// Firma de metodos para buscar todos
        /// </summary>
        /// <returns>listado de la entidad indicada</returns>
        System.Threading.Tasks.Task<IList<TEntity>> SearchAllAsync();
    }
}
