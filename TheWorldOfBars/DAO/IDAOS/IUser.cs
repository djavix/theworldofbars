﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAO.IDAOS
{
    public interface IUser : IBase<Common.Entities.User>
    {
        System.Threading.Tasks.Task<Common.Entities.User> Login(Common.Entities.User user);
    }
}
