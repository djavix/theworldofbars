﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTest
{
    [TestClass]
    public class DAOTest
    {
        [TestMethod]
        public async void AddUser()
        {
            Common.Entities.User userAdmin = new Common.Entities.User();
            userAdmin.NickName = "admin";
            userAdmin.Password = "Abc23456";
            userAdmin.FullName = "Javier Velasquez";
            userAdmin.Birthday = new System.DateTime(1992, 7, 17);
            userAdmin.Email = "admin@admin.com";

            DAO.IDAOS.IUser dao = DAO.Factory.GetDAOUser();
            bool verify = await dao.AddAsync(userAdmin);

            Assert.IsTrue(verify);
        }

        [TestMethod]
        public async void SelectAllUser()
        {

            DAO.IDAOS.IUser dao = DAO.Factory.GetDAOUser();
            IList<Common.Entities.User> users = await dao.SearchAllAsync();

            Assert.IsTrue(users.Count > 0);
        }
    }
}
