﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TheWorldOfBarsMovile.Services;

namespace TheWorldOfBarsMovile.ViewModel
{
    public class MenuItemViewModel
    {
        private string _icon;
        private string _title;
        private string _pageName;
        private NavigationServices _navigation;

        #region Construct
        public MenuItemViewModel()
        {
            _navigation = new NavigationServices();
        }
        #endregion

        #region Properties
        public string Icon { get => _icon; set => _icon = value; }
        public string Title { get => _title; set => _title = value; }
        public string PageName { get => _pageName; set => _pageName = value; }
        #endregion

        #region Commands
        public ICommand NavigateCommand { get { return new RelayCommand(Navigate); } }

        private async void Navigate()
        {
            await _navigation.Navigate(this.PageName);
        }
        #endregion
    }
}
