﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace TheWorldOfBarsMovile.ViewModel
{
    public class MainViewModel
    {
        #region Variables
        private ObservableCollection<MenuItemViewModel> _menu;
        private LoginViewModel _login;
        private UserRegisterViewModel _userRegister;
        #endregion

        #region Properties
        public ObservableCollection<MenuItemViewModel> Menu { get => _menu; set => _menu = value; }
        public LoginViewModel Login { get => _login; set => _login = value; }
        public UserRegisterViewModel UserRegister { get => _userRegister; set => _userRegister = value; }

        #endregion

        #region Constructor
        public MainViewModel()
        {
            _menu = new ObservableCollection<MenuItemViewModel>();
            _login = new LoginViewModel();
            _userRegister = new UserRegisterViewModel();
            LoadMenu();
        }
        #endregion

        #region Private Methods
        private MenuItemViewModel CreateMenuItem(string icon, string title, string pageName)
        {
            return new MenuItemViewModel() { Icon = icon, Title = title, PageName = pageName };
        }
        private void LoadMenu()
        {
            _menu.Add(CreateMenuItem("ic_action_mybar.png", "My Bar", "MyBarPage"));
            _menu.Add(CreateMenuItem("ic_action_socialbar.png", "Social Bars", "SocialBarPage"));
            _menu.Add(CreateMenuItem("ic_action_store.png", "Store", "StorePage"));
            _menu.Add(CreateMenuItem("ic_action_myprofile.png", "My Profile", "MyProfilePage"));
            _menu.Add(CreateMenuItem("ic_action_exit.png", "Exit", "Exit"));
        }
        #endregion
    }
}
