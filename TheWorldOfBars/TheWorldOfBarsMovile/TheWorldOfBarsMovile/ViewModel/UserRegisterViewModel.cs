﻿using Common.Entities;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TheWorldOfBarsMovile.Services;

namespace TheWorldOfBarsMovile.ViewModel
{
    public class UserRegisterViewModel: Presenter.IPresenters.IUser
    {
        #region Attributes
        private string _nickName;
        private string _password;
        private string _fullName;
        private string _email;
        private string _birthday;
        private NavigationServices _navigation;
        private DialogServices _dialog;
        private string _message;
        private Common.Entities.User _user;
        private Presenter.Presenters.User _presenter;
        #endregion

        #region Propertties
        public string NickName { get => _nickName; set => _nickName = value; }
        public string Password { get => _password; set => _password = value; }
        public string FullName { get => _fullName; set => _fullName = value; }
        public string Email { get => _email; set => _email = value; }
        public string Birthday { get => _birthday; set => _birthday = value; }
        #endregion

        #region Construct
        public UserRegisterViewModel()
        {
            _navigation = new NavigationServices();
            _dialog = new DialogServices();
            _presenter = new Presenter.Presenters.User(this);
        }
        #endregion

        #region Commands
        public ICommand Save { get { return new RelayCommand(SaveUser); } }
        public ICommand Cancel { get { return new RelayCommand(CancelRegister); } }

        public User User { get => new User() { NickName = _nickName, Password = _password, FullName = _fullName, Email = _email, Birthday = Convert.ToDateTime(_birthday) }; set => _user = value; }
        public string Message { set => _message = value; }

        private void CancelRegister()
        {
            _navigation.CancelRegister();
        }

        private async void SaveUser()
        {
            if(await _presenter.Save())
            {
                _navigation.Acces();
            }
            else
            {
                await _dialog.ShowMessage("Error", _message);
            }
        }
        #endregion
    }
}
