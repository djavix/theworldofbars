﻿using Common.Entities;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TheWorldOfBarsMovile.Services;

namespace TheWorldOfBarsMovile.ViewModel
{
    public class LoginViewModel : Presenter.IPresenters.IUser
    {

        #region Attributes
        private string _nickName;
        private string _password;
        private bool _isRemerbered;
        private NavigationServices _navigation;
        private DialogServices _dialog;
        private string _message;
        private Common.Entities.User _user;
        private Presenter.Presenters.User _presenter;
        #endregion

        #region Properties
        public string NickName { get => _nickName; set => _nickName = value; }
        public string Password { get => _password; set => _password = value; }
        public bool IsRemerbered { get => _isRemerbered; set => _isRemerbered = value; }
        #endregion

        #region Construct
        public LoginViewModel()
        {
            _navigation = new NavigationServices();
            _dialog = new DialogServices();
            _presenter = new Presenter.Presenters.User(this);
        }
        #endregion

        #region Commands
        public ICommand LoginCommand { get { return new RelayCommand(Login); } }
        public ICommand RegisterCommand { get { return new RelayCommand(GoToRegister); } }

        #region Properties Interface
        public User User { get => new Common.Entities.User() { NickName = _nickName, Password = _password}; set => _user = value; }
        public string Message { set => _message = value; } 
        #endregion

        private void GoToRegister()
        {
            _navigation.GoToRegisterUser();
        }

        private async  void Login()
        {
            if (string.IsNullOrEmpty(NickName))
                await _dialog.ShowMessage("Error", "Entry Nick Name don't empty.");
            else
                _navigation.Acces();
        }
        #endregion
    }
}
