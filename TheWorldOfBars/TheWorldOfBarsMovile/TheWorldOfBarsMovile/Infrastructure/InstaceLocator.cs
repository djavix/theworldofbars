﻿using System;
using System.Collections.Generic;
using System.Text;
using TheWorldOfBarsMovile.ViewModel;

namespace TheWorldOfBarsMovile.Infrastructure
{
    public class InstaceLocator
    {
        private MainViewModel _main;

        public MainViewModel Main { get => _main; set => _main = value; }

        public InstaceLocator()
        {
            _main = new MainViewModel();
        }
    }
}
