﻿using System;
using TheWorldOfBarsMovile.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TheWorldOfBarsMovile
{
    public partial class App : Application
    {
        #region Properties
        public static NavigationPage Navigator { get; internal set; }
        public static MasterPage Master { get; internal set; }
        #endregion

        #region Construct
        public App()
        {
            InitializeComponent();

            MainPage = new Pages.LoginPage();
        }
        #endregion

        #region Methods
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        } 
        #endregion
    }
}
