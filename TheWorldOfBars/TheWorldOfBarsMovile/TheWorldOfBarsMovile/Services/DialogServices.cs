﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TheWorldOfBarsMovile.Services
{
    public class DialogServices
    {
        public async Task ShowMessage(string title, string message)
        {
            await App.Current.MainPage.DisplayAlert(title, message, "OK");
        }
    }
}
