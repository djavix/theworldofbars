﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TheWorldOfBarsMovile.Pages;
using Xamarin.Forms;

namespace TheWorldOfBarsMovile.Services
{
    public class NavigationServices
    {
        public async Task Navigate(string pageName)
        {
            App.Master.IsPresented = false;
            switch (pageName)
            {                
                case "MyBarPage":
                    await App.Navigator.PopToRootAsync();
                    break;
                case "MyProfilePage":
                    await Navigate(new MyProfilePage());
                    break;
                case "SocialBarPage":
                    await Navigate(new SocialBarPage());
                    break;
                case "StorePage":
                    await Navigate(new StorePage());
                    break;
                case "Exit":
                    ExitUser();
                    break;
                default:
                    break;
            }
        }

        private void ExitUser()
        {
            App.Current.MainPage = new LoginPage();
        }

        private async Task Navigate<T>(T page) where T : Page
        {
            NavigationPage.SetHasBackButton(page, false);
            await App.Navigator.PushAsync(page);
        }

        public void Acces()
        {
            App.Current.MainPage = new MasterPage();
        }

        internal void CancelRegister()
        {
            App.Current.MainPage = new LoginPage();
        }

        internal void GoToRegisterUser()
        {
            App.Current.MainPage = new UserRegisterPage();
        }
    }
}
