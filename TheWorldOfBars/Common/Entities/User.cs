﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.API;

namespace Common.Entities
{
    public class User : Entity
    {
        private string _nickName;
        private string _password;
        private string _fullName;
        private DateTime _birthday;
        private string _email;
        private string _biography;
        private string _photo;
        private IList<User> _followers;
        private IList<User> _following;
        private IList<Glassware> _myGlasses;
        private IList<Cocktail> _myRecipes;
        private IList<Cocktail> _otherRecipes;
        private IList<Cocktail> _cocktailsLikeIt;
        private IList<Ingredient> _myIngredients;

        public static explicit operator User(RUser v)
        {
            User result = null;
            if (v != null)
            {
                result = new User();
                result.Id = v.Id;
                result.NickName = v.NickName;
                result.FullName = v.FullName;
                result.Birthday = v.Birthday;
                result.Email = v.Email;
                result.Biography = v.Biography;
                result.Photo = v.Photo;
            }
            return result;
        }

        public virtual string NickName { get => _nickName; set => _nickName = value; }
        public virtual string Password { get => _password; set => _password = value; }
        public virtual string FullName { get => _fullName; set => _fullName = value; }
        public virtual DateTime Birthday { get => _birthday; set => _birthday = value; }
        public virtual string Email { get => _email; set => _email = value; }
        public virtual string Biography { get => _biography; set => _biography = value; }
        public virtual string Photo { get => _photo; set => _photo = value; }
        public virtual IList<User> Followers { get => _followers; set => _followers = value; }
        public virtual IList<User> Following { get => _following; set => _following = value; }
        public virtual IList<Glassware> MyGlasses { get => _myGlasses; set => _myGlasses = value; }
        public virtual IList<Cocktail> MyRecipes { get => _myRecipes; set => _myRecipes = value; }
        public virtual IList<Cocktail> OtherRecipes { get => _otherRecipes; set => _otherRecipes = value; }
        public virtual IList<Cocktail> CocktailsLikeIt { get => _cocktailsLikeIt; set => _cocktailsLikeIt = value; }
        public virtual IList<Ingredient> MyIngredients { get => _myIngredients; set => _myIngredients = value; }

        public User() { }
    }
}
