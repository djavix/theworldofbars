﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Cocktail : Entity
    {
        private string _name;
        private string _photo;
        private string _description;
        private string _preparation;
        private User _creator;
        private Glassware _glass;
        private List<User> _whoLike;
        private List<Commentary> _commentaries;
        private IList<CocktailIngredient> _ingredients;

        public virtual string Name { get => _name; set => _name = value; }
        public virtual string Photo { get => _photo; set => _photo = value; }
        public virtual string Description { get => _description; set => _description = value; }
        public virtual string Preparation { get => _preparation; set => _preparation = value; }
        public virtual User Creator { get => _creator; set => _creator = value; }
        public virtual Glassware Glass { get => _glass; set => _glass = value; }
        public virtual List<User> WhoLike { get => _whoLike; set => _whoLike = value; }
        public virtual List<Commentary> Commentaries { get => _commentaries; set => _commentaries = value; }
        public virtual IList<CocktailIngredient> Ingredients { get => _ingredients; set => _ingredients = value; }

        public Cocktail() { }
    }
}
