﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Glassware : Entity
    {
        private string _name;
        private string _description;
        private string _photo;

        public virtual string Name { get => _name; set => _name = value; }
        public virtual string Description { get => _description; set => _description = value; }
        public virtual string Photo { get => _photo; set => _photo = value; }

        public Glassware() { }
    }
}
