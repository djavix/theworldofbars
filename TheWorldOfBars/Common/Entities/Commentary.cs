﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Commentary : Entity
    {
        private string _comment;
        private DateTime _date;
        private User _from;
        private Cocktail _for;
        private IList<User> _whoLike;

        public virtual string Comment { get => _comment; set => _comment = value; }
        public virtual DateTime Date { get => _date; set => _date = value; }
        public virtual User From { get => _from; set => _from = value; }
        public virtual Cocktail For { get => _for; set => _for = value; }
        public virtual IList<User> WhoLike { get => _whoLike; set => _whoLike = value; }

        public Commentary() { }
    }
}
