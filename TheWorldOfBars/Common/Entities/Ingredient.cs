﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Ingredient : Entity
    {
        private string _name;
        private string _description;
        private string _photo;
        private TypeIngredient _type;
        private IList<Measure> _measure;
        

        public virtual string Name { get => _name; set => _name = value; }
        public virtual string Description { get => _description; set => _description = value; }
        public virtual string Photo { get => _photo; set => _photo = value; }
        public virtual TypeIngredient Type { get => _type; set => _type = value; }
        public virtual IList<Measure> Measure { get => _measure; set => _measure = value; }

        public Ingredient() { }
    }
}
