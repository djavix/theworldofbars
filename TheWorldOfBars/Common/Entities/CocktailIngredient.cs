﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class CocktailIngredient : Entity
    {
        private float _quantity;
        private bool _optional;
        private bool _garnish;
        private Cocktail _cocktail;
        private Ingredient _ingredient;
        private Measure _measure;

        public virtual float Quantity { get => _quantity; set => _quantity = value; }
        public virtual bool Optional { get => _optional; set => _optional = value; }
        public virtual bool Garnish { get => _garnish; set => _garnish = value; }
        public virtual Cocktail Cocktail { get => _cocktail; set => _cocktail = value; }
        public virtual Ingredient Ingredient { get => _ingredient; set => _ingredient = value; }
        public virtual Measure Measure { get => _measure; set => _measure = value; }

        public CocktailIngredient() { }
    }
}
