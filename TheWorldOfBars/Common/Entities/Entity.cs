﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Entity
    {
        private int _id;

        public virtual int Id { get => _id; set => _id = value; }

        public Entity() { }
    }
}
