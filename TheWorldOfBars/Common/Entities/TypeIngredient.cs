﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class TypeIngredient : Entity
    {
        private string _name;
        private string _description;

        public virtual string Name { get => _name; set => _name = value; }
        public virtual string Description { get => _description; set => _description = value; }

        public TypeIngredient() { }
    }
}
