﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Entities;

namespace Common.API
{
    public class RUser : REntity
    {
        private string _nickName;
        private string _password;
        private string _fullName;
        private DateTime _birthday;
        private string _email;
        private string _biography;
        private string _photo;

        public string NickName { get => _nickName; set => _nickName = value; }
        public string Password { get => _password; set => _password = value; }
        public string FullName { get => _fullName; set => _fullName = value; }
        public DateTime Birthday { get => _birthday; set => _birthday = value; }
        public string Email { get => _email; set => _email = value; }
        public string Biography { get => _biography; set => _biography = value; }
        public string Photo { get => _photo; set => _photo = value; }

        public RUser() { }

        public static explicit operator RUser(User v)
        {
            RUser result = null;
            if(v != null)
            {
                result = new RUser();
                result.Id = v.Id;
                result.NickName = v.NickName;
                result.FullName = v.FullName;
                result.Birthday = v.Birthday;
                result.Email = v.Email;
                result.Biography = v.Biography;
                result.Photo = v.Photo;
            }
            return result;
        }
    }
}
