﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API
{
    public static class Result<Object>
    {
        public static ActionResult<Common.API.Response<Object>> New(Common.API.Response<Object> response)
        {
            return new ActionResult<Common.API.Response<Object>>(response);
        }
    }
}
