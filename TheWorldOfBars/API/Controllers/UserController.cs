﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private Controller.API.User _user;

        public UserController()
        {
            _user = new Controller.API.User();
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult<Common.API.Response<Common.API.RUser>>> Add(Common.API.RUser user)
        {
            Common.Entities.User userSave = (Common.Entities.User)user;
            return Result<Common.API.RUser>.New(await _user.GetResponseAdd(userSave));
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<ActionResult<Common.API.Response<Common.API.RUser>>> Login(Common.Entities.User user)
        {
            return Result<Common.API.RUser>.New(await _user.GetResponseLogin(user));
        }

        [HttpGet]
        public async Task<ActionResult<Common.API.Response<Common.API.RUser>>> Users()
        {
            return Result<Common.API.RUser>.New(await _user.GetResponseSearchAll());
        }
    }
}